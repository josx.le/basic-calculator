import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {


        /*Scanner */
        Scanner leer = new Scanner(System.in);

        /*Variables */

        int opcion;

        int numero1, numero2, resultado;

        /*Pregunta que desea hacer */

        System.out.println("\n");
        System.out.println("Calculadora Básica! \n");
        
        System.out.println("Que opción desea realizar: ");
        
        System.out.println("1) Sumar ");
        System.out.println("2) Restar ");
        System.out.println("3) Multiplicar ");
        System.out.println("4) Dividir  \n");

        System.out.print("Opción: ");
        opcion = leer.nextInt();

        /*Suma */

        if(opcion == 1) {
            System.out.print("Ingrese el primer número que desea sumar:  ");
            numero1 = leer.nextInt();
            System.out.print("Ingrese el segundo número que desea sumar: ");
            numero2 = leer.nextInt();

            resultado = numero1 + numero2;

            System.out.println("Su resultado es: " + resultado);
        }

        /*Resta */

        if(opcion == 2) {
            System.out.print("Ingrese el primer número que desea restar:  ");
            numero1 = leer.nextInt();
            System.out.print("Ingrese el segundo número que desea restar: ");
            numero2 = leer.nextInt();

            resultado = numero1 - numero2;

            System.out.println("Su resultado es: " + resultado);
        }

        /*Multiplicación */

        if(opcion == 3) {
            System.out.print("Ingrese el primer número que desea multiplicar:  ");
            numero1 = leer.nextInt();
            System.out.print("Ingrese el segundo número que desea multiplicar: ");
            numero2 = leer.nextInt();

            resultado = numero1 * numero2;

            System.out.println("Su resultado es: " + resultado);
        }

        /*División */

        if(opcion == 4) {
            System.out.print("Ingrese el primer número que desea dividir:  ");
            numero1 = leer.nextInt();
            System.out.print("Ingrese el segundo número que desea dividir: ");
            numero2 = leer.nextInt();

            resultado = numero1 / numero2;

            System.out.println("Su resultado es: " + resultado);
        }

    }
}
